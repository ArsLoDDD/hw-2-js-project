$('.header__menu-container-button').click(function (e) {
	$('.header__menu-container-button').toggleClass('active')
	if ($('.header__menu-container-button').hasClass('active')) {
		$('.header__menu-container-button-content').css({
			transform: 'rotate(90deg)',
		})
		setTimeout(() => {
			$('.header__menu-container-button-content').css({
				backgroundImage: "url('./../img/close.svg')",
			})
		}, 50)
	} else {
		$('.header__menu-container-button-content').css({
			transform: 'rotate(-180deg)',
		})
		setTimeout(() => {
			$('.header__menu-container-button-content').css({
				backgroundImage: "url('./../img/menu.svg')",
			})
		}, 50)
	}
	$('.header__menu-container-menu').toggleClass('active')
})
